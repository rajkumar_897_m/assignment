$(document).ready(function(){

	var datatable;
	$(document).on('click','.view-btn', function(){
		$('#view_name').html($(this).attr('student_name'));
		$('#view_subject').html($(this).attr('subject'));
		$('#view_marks').html($(this).attr('marks'));
		$('#viewModal').modal();
	});

	$(document).on('click','.edit-btn', function(){
		$('#name').val($(this).attr('student_name'));
		$('#subject').val($(this).attr('subject'));
		$('#marks').val($(this).attr('marks'));
		$('#document_id').val($(this).attr('document_id'));
		$('#editModal').modal();
	});

	$('#submit-edit-form').click(function(){
		var name = $('#name').val();
		var subject = $('#subject').val();
		var marks = $('#marks').val();
		var document_id = $('#document_id').val();
		$.ajax({
	         type: "POST",
	         url: site_url+"api/students/update",
    		 cache : false,
	         data: {document_id:document_id,name:name,subject:subject,marks:marks},
	         success:  function(result){
	         	if(result==1)
	         	{
	         		swal("", "Record has been updated!", "success");
		         	datatable.destroy();
		         	getStudents();
		         }else{

	         		swal("", "Something went wrong, Please try again!", "error");
		         }
	         	
	         }
	     });
	});

	$(document).on('click','.delete-btn', function(){
		var document_id = $(this).attr('document_id');
		$.ajax({
	         type: "POST",
	         url: site_url+"students/delete",
    		 cache : false,
	         data: {document_id:document_id},
	         success:  function(result){
	         	if(result==1)
	         	{
		         	swal("", "Record has been deleted!", "success");
		         	datatable.destroy();
		         	getStudents();
	         	}else{

	         		swal("", "Something went wrong, Please try again!", "error");
		         }
	         }
	     });
	});

	$('.filter').click(function(){
		datatable.destroy();
		getStudents();
	});

	function renderData(res)
	{
		var html = '';
     	for(var i=0;i<res.length;i++)
     	{
     		html += '<tr>';
     		html += '<td>'+(i+1)+'</td>';
     		html += '<td>'+res[i].Name+'</td>';
     		html += '<td>'+res[i].Subject+'</td>';
     		html += '<td>'+res[i].Marks+'</td>';
     		html += '<td><button class="btn btn-primary view-btn" student_name="'+res[i].Name+'" subject="'+res[i].Subject+'" marks="'+res[i].Marks+'" style="margin-right:5px"><i class="fa fa-eye"></i></button>';
     		html += '<button class="btn btn-warning edit-btn" student_name="'+res[i].Name+'" subject="'+res[i].Subject+'" marks="'+res[i].Marks+'" document_id="'+res[i]._id.$id+'" style="margin-right:5px"><i class="fa fa-edit"></i></button>';
            html += '<button class="btn btn-danger delete-btn" student_name="'+res[i].Name+'" subject="'+res[i].Subject+'" document_id="'+res[i]._id.$id+'"><i class="fa fa-trash"></i></button>';
            html += '</td></tr>';
     	}
     	$('#students_list').html(html);
	}

	function getStudents()
	{
		var marks = $('#filter_marks').val();
	  datatable = $('#students').DataTable({
	     "processing": true, //Feature control the processing indicator.
	     "serverSide": true, //Feature control DataTables' 
	     "order": [], //Initial no order.
	     "pageLength": 10, // Set Page Length
	     // Load data for the table's content from an Ajax source
	     "ajax": {
	         "url": site_url+"students/getStudents",
	         "type": "POST",
	         //You can post any data from here. If you do not need then remove it.
	         "data": {"marks": marks}
	          
	     },
	      
	     //Set column definition initialisation properties.
	     "columnDefs": [
	         {
	             "targets": [0,4], //first, fourth & seventh column
	             "orderable": false //set not orderable
	         }
	     ],
	    "fnInitComplete": function (oSettings, response) {
	     // here you to do after load data
	     //response.recordsTotal is used to get Total count data
	     $("#countData").text(response.recordsTotal);
	   }
	      
	  });
	}
	getStudents();
});