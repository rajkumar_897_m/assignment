<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Students</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h4>Filter :</h4>
          <div class="row">
            <!-- <div class="form-group col-sm-6">
              <label for="inputName">Search</label>
              <input type="text" name="tag" class="form-control" id="filter_tag">
            </div> -->
            <div class="form-group col-sm-12">
              <label for="inputStatus">Marks</label>
              <select class="form-control custom-select" name="marks" id="filter_marks">
                <option value="all">All</option>
                <option value="90-100">More than 90</option>
                <option value="80-90">80 - 90</option>
                <option value="70-80">70 - 80</option>
                <option value="60-70">60 - 70</option>
                <option value="0-60">Less than 60</option>

              </select>
            </div>
            <button class="btn btn-success filter" style="margin-left: 15px">Filter</button>
          </div>
          <div class="card-tools">
            <a href="<?= site_url()?>students/addStudent" class="btn btn-tool">
              <i class="fas fa-plus"></i> Add Student</a>
          </div>
        </div>
        <div class="card-body p-0">
          <table id="students" class="dataTable table table-striped projects">
              <thead style="text-align: left;">
                  <tr>
                      <th>
                          Sno
                      </th>
                      <th>
                          Name
                      </th>
                      <th>
                          Subject
                      </th>
                      <th>
                          Marks
                      </th>
                      <th>
                          Actions
                      </th>
                  </tr>
              </thead>
          </table>
      </div>
  </div>
</section>
</div>

  <!-- The Modal -->
  <div class="modal" id="viewModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">View Student</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-body">
                  <table class="table table-striped">
                    <tr>
                      <th>Student Name</th>
                      <td id="view_name"></td>
                    </tr>
                    <tr>
                      <th>Subject</th>
                      <td id="view_subject"></td>
                    </tr>
                    <tr>
                      <th>Marks</th>
                      <td id="view_marks"></td>
                    </tr>
                  </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

  <!-- The Modal -->
  <div class="modal" id="editModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Student</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-body">
                <input type="hidden" id="document_id" value="">
                  <div class="form-group col-sm-12">
                    <label for="inputName">Student Name</label>
                    <input type="text" name="name" class="form-control" id="name" required="">
                  </div>
                  <div class="form-group col-sm-12">
                    <label for="inputName">Subject</label>
                    <input type="text" name="subject" class="form-control" id="subject" required="">
                  </div>
                  <div class="form-group col-sm-12">
                    <label for="inputName">Marks</label>
                    <input type="number" name="marks" class="form-control" id="marks" required="">
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" id="submit-edit-form">Submit</button>
        </div>
        
      </div>
    </div>
  </div>
  
<script type="text/javascript">
  var site_url = '<?= site_url()?>';
</script>
<script type="text/javascript" src="<?= site_url()?>assets/js/students.js"></script>