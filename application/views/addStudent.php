  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Student</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-body">
            	<form action="<?= site_url()?>students/add" method="POST" class="row">
	              <div class="form-group col-sm-6">
	                <label for="inputName">Student Name</label>
	                <input type="text" name="name" class="form-control" required="">
	              </div>
	              <div class="form-group col-sm-6">
	                <label for="inputName">Subject</label>
	                <input type="text" name="subject" class="form-control" required="">
	              </div>
	              <div class="form-group col-sm-6">
	                <label for="inputName">Marks</label>
	                <input type="number" name="marks" class="form-control" required="">
	              </div>
	              <div class="col-12">
			          <input type="submit" value="Add" class="btn btn-success float-right">
			       </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </div>
</section>
</div>