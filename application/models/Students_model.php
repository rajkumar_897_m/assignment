<?php
class Students_model extends CI_Model
{
	function getStudents($post){
		if(!empty($post['search_value']))
        {
        	$search = $post['search_value'];
        	$condtion = array(
        		'Name'=> new MongoDB\BSON\Regex($search),
                'Subject' => new MongoDB\BSON\Regex($search),
                'Marks'  => new MongoDB\BSON\Regex($search)
                );
			$this->mongo_db->where_or($condtion);
        }
        $marks = $post['marks'];
		if($marks!='all')
		{
			$from = explode('-', $marks)[0];
			$to = explode('-', $marks)[1];
			$this->mongo_db->where_gte('Marks',$from);
			$this->mongo_db->where_lte('Marks',$to);
		}
        if (!empty($post['order'])) {
             
            $this->mongo_db->order_by(array($post['column_order'][$post['order'][0]['column']] => $post['order'][0]['dir']));
             
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->mongo_db->order_by(array(key($order) => $order[key($order)]));
             
        }
        if ($post['length'] != -1) {
        	$this->mongo_db->limit($post['length'])->offset($post['start']);
        }
        $result = $this->mongo_db->get('students');
        return $result;
        // echo '<pre>'; print_r($result); die();
    }
     
     
    function count_all($post){
        $count = $this->mongo_db->count('students');
        return $count;
    }
     
    function count_filtered($post){
        if(!empty($post['search_value']))
        {
        	$search = $post['search_value'];
        	$condtion = array(
        		'Name'=> new MongoDB\BSON\Regex($search),
                'Subject' => new MongoDB\BSON\Regex($search),
                'Marks'  => new MongoDB\BSON\Regex($search)
                    
                );
			$this->mongo_db->where_or($condtion);
        }
        $marks = $post['marks'];
		if($marks!='all')
		{
			$from = explode('-', $marks)[0];
			$to = explode('-', $marks)[1];
			$this->mongo_db->where_gte('Marks',$from);
			$this->mongo_db->where_lte('Marks',$to);
		}
        $count = $this->mongo_db->count('students');
        return $count;
    }

	public function add($input)
	{
		$this->mongo_db->where(array('Name' => $input['name'],'Subject' => $input['subject']));
		$result = $this->mongo_db->get('students');
		if(sizeof($result)!=0)
		{
			$data = array(
				'Name' => $input['name'],
				'Subject' => $input['subject'],
				'Marks' => $result[0]['Marks']+$input['marks']
			);
			$this->mongo_db->where(array('Name' => $input['name'],'Subject' => $input['subject']));
			$this->mongo_db->set($data);
			$this->mongo_db->update('students');

		}else{
			$data = array(
				'Name' => $input['name'],
				'Subject' => $input['subject'],
				'Marks' => $input['marks']
			);
			$this->mongo_db->insert('students', $data);
		}
		return;
	}

	public function update($input){
		$data = array(
			'Name' => $input['name'],
			'Subject' => $input['subject'],
			'Marks' => $input['marks']
		);
		$id = $input['document_id'];
		$this->mongo_db->where(['_id' => new MongoDB\BSON\ObjectId($id)]);
		$this->mongo_db->set($data);
		$status = $this->mongo_db->update('students');
		return 1;
	}

	public function delete($input)
	{
		$id = $input['document_id'];
		$this->mongo_db->where(['_id' => new MongoDB\BSON\ObjectId($id)]);
		$status = $this->mongo_db->delete('students');
		return 1;
	}
}
?>