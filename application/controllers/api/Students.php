<?php
require APPPATH.'libraries/REST_Controller.php';
class students extends REST_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('students_model');
	}
	public function update()
	{
		if($this->session->userdata('user'))
		{
			$input = $this->input->post();
			$status = $this->students_model->update($input);
			echo $status;
		}else{
			redirect(site_url());
		}
	} 
}