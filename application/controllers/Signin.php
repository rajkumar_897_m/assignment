<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('signin_model');
	}

	public function index()
	{
		$input = $this->input->post();
		$status = $this->signin_model->verify($input);
		if($status==1)
		{
			$this->session->set_userdata('user',$input['username']);
			redirect('students');
		}else{
			$this->session->set_flashdata('signin_error',1);
			$this->load->view('index');
		}
	}
}