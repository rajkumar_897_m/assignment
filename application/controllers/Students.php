<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('students_model');
	}

	public function index()
	{
		if($this->session->userdata('user'))
		{
			$data['students'] = array();
			// $data['students'] = $this->students_model->getStudents();
			$data['main_content'] = 'students';
			$this->load->view('templates/index',$data);
			// echo '<pre>'; print_r($data); die();
		}else{
			redirect(site_url());
		}
	}

	public function getStudents(){
       $post = array();
       $post['length'] = $this->input->post('length');
        $post['start'] = $this->input->post('start');
        $search = $this->input->post('search');
        $post['search_value'] = $search['value'];
        $post['order'] = $this->input->post('order');
        $post['draw'] = $this->input->post('draw');
        $post['status'] = $this->input->post('status');
        $post['marks'] = $this->input->post('marks');
        $post['column_order'] = array( null,'Name','Subject','Marks');
        $post['column_search'] = array('Name','Subject', 'Marks');
         
        $list = $this->students_model->getStudents($post);
        $data = array();
        $no = $post['start'];
         
        foreach ($list as $student) {
            $no++;
            $row =  $this->getStudents_table_data($student, $no);
            $data[] = $row;
        }
         
        $data = array(
                'data' => $data,
                'post' => $post
                );
          $output = array(
            "draw" => $post['draw'],
            "recordsTotal" => $this->students_model->count_all($post),
            "recordsFiltered" =>  $this->students_model->count_filtered($post),
            "data" => $data['data'],
        );
        unset($post);
        unset($data);
        echo json_encode($output);
         
    }

    function getStudents_table_data($student, $no){
        $row = array();
        $row[] = $no; 
        $row[] = $student['Name'];
        $row[] = $student['Subject'];
        $row[] = $student['Marks'];
        $name = $student['Name'];
        $subject = $student['Subject'];
        $marks = $student['Marks'];
        $i = '$id';
        $id = $student['_id']->$i;
        $row[] = '<button class="btn btn-primary view-btn" student_name="'.$name.'" subject="'.$subject.'" marks="'.$marks.'"><i class="fa fa-eye"></i></button>
            <button class="btn btn-warning edit-btn" student_name="'.$name.'" subject="'.$subject.'" marks="'.$marks.'" document_id="'.$id.'"><i class="fa fa-edit"></i></button>
            <button class="btn btn-danger delete-btn" student_name="'.$name.'" subject="'.$subject.'" document_id="'.$id.'"><i class="fa fa-trash"></i></button>';;
        return $row;
    }

	public function addStudent()
	{
		if($this->session->userdata('user'))
		{
			$data['main_content'] = 'addStudent';
			$this->load->view('templates/index',$data);
		}else{
			redirect(site_url());
		}
	}

	public function add()
	{
		if($this->session->userdata('user'))
		{
			$input = $this->input->post();
			$status = $this->students_model->add($input);
			redirect('students');
		}else{
			redirect(site_url());
		}
	}

	public function delete()
	{
		if($this->session->userdata('user'))
		{
			$input = $this->input->post();
			$status = $this->students_model->delete($input);
			echo $status;
		}else{
			redirect(site_url());
		}
	}
}