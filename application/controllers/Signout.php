<?php
class signout extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

	public function index(){
		$this->session->sess_destroy();
	    redirect(site_url());
	}
}

?>